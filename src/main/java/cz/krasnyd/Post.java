package cz.krasnyd;

import java.util.Date;

public class Post {

    private int likesCount;
    private String type;
    private int commentsCount;
    private String caption = "";
    private Date createDate;
    private String thumbnailImage;

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    @Override
    public String toString() {
        return "Post{" +
                "likesCount=" + likesCount +
                ", type='" + type + '\'' +
                ", commentsCount=" + commentsCount +
                ", caption='" + caption + '\'' +
                ", createDate=" + createDate +
                ", thumbnailImage='" + thumbnailImage + '\'' +
                '}';
    }
}
