package cz.krasnyd;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    public Main() throws Exception {
        User u = getUser("taylorswift");
        System.out.println(u);
        List<Post> posts = getUserPosts(u.getId());
        for (Post p : posts) {
            System.out.println(p);
        }
    }

    public User getUser(String username) throws Exception {
        System.out.println("USER PROCESSING");
        HttpGet post = new HttpGet("https://www.instagram.com/" + username + "/?__a=1");

        try (CloseableHttpResponse response = httpClient.execute(post)) {
            String responseBody = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            //System.out.println(responseBody);
            JSONObject obj = new JSONObject(responseBody);
            JSONObject userInfo = obj.getJSONObject("graphql").getJSONObject("user");

            User u = new User();
            u.setBio(userInfo.getString("biography"));
            u.setExternalUrl(userInfo.getString("external_url"));
            u.setFollowersCount(userInfo.getJSONObject("edge_followed_by").getInt("count"));
            u.setFullName(userInfo.getString("full_name"));
            u.setId(userInfo.getInt("id"));
            u.setPostsCount(userInfo.getJSONObject("edge_owner_to_timeline_media").getInt("count"));
            u.setProfilePicture(userInfo.getString("profile_pic_url"));
            u.setUsername(userInfo.getString("username"));
            u.setVerified(userInfo.getBoolean("is_verified"));
            System.out.println("USER DONE");
            return u;
        }
    }

    public List<Post> getUserPosts(int userId) throws URISyntaxException, IOException {
        System.out.println("POSTS PROCESSING");
        List<Post> posts = new ArrayList<>();
        String after = null;
        boolean hasNext = true;

        // prochází všechny posty po dávkách (v každé dávce je max 50 postů)
        while (hasNext) {
            URIBuilder builder = new URIBuilder("https://www.instagram.com/graphql/query/");
            builder.setParameter("query_hash", "003056d32c2554def87228bc3fd9668a");
            JSONObject requestParams = new JSONObject();
            requestParams.put("id", userId);
            requestParams.put("first", 50);
            if (after != null) {
                requestParams.put("after", after);
            }
            builder.setParameter("variables", requestParams.toString());
            HttpGet get = new HttpGet(builder.build());
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                String responseBody = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
                //System.out.println(responseBody);

                JSONObject obj = new JSONObject(responseBody);
                JSONObject pageInfo = obj.getJSONObject("data").getJSONObject("user").getJSONObject("edge_owner_to_timeline_media").getJSONObject("page_info");
                hasNext = pageInfo.getBoolean("has_next_page");
                if (hasNext) {
                    after = pageInfo.getString("end_cursor");
                }

                JSONArray postsData = obj.getJSONObject("data").getJSONObject("user").getJSONObject("edge_owner_to_timeline_media").getJSONArray("edges");
                for (int i = 0; i < postsData.length(); i++) {
                    JSONObject item = postsData.getJSONObject(i).getJSONObject("node");
                    Post post = new Post();

                    post.setLikesCount(item.getJSONObject("edge_media_preview_like").getInt("count"));
                    post.setType(item.getString("__typename"));
                    post.setCommentsCount(item.getJSONObject("edge_media_to_comment").getInt("count"));

                    JSONArray arr = item.getJSONObject("edge_media_to_caption").getJSONArray("edges");
                    for (int b = 0; b < arr.length(); b++) {
                        post.setCaption(post.getCaption() + arr.getJSONObject(b).getJSONObject("node").getString("text"));
                    }

                    post.setCreateDate(new Date(item.getLong("taken_at_timestamp") * 1000));
                    post.setThumbnailImage(item.getString("thumbnail_src"));
                    posts.add(post);
                }
            }
        }

        System.out.println("POSTS DONE");
        return posts;
    }

    public static void main(String[] args) throws Exception {
        new Main();
    }
}
