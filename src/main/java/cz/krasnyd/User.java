package cz.krasnyd;

public class User {
    private int id;
    private String bio;
    private int followersCount;
    private String profilePicture;
    private String username;
    private String fullName;
    private String externalUrl;
    private int postsCount;
    private boolean isVerified;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public int getPostsCount() {
        return postsCount;
    }

    public void setPostsCount(int postsCount) {
        this.postsCount = postsCount;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", bio='" + bio + '\'' +
                ", followersCount=" + followersCount +
                ", profilePicture='" + profilePicture + '\'' +
                ", username='" + username + '\'' +
                ", fullName='" + fullName + '\'' +
                ", externalUrl='" + externalUrl + '\'' +
                ", postsCount=" + postsCount +
                ", isVerified=" + isVerified +
                '}';
    }
}
